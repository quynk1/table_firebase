import React from 'react';
import Languages from 'components/languages';
import type { NextPage } from 'next';

const Home: NextPage = () => {
  return (
    <Languages />
  );
};

export default Home;
