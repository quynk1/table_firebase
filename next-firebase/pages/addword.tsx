import React, { useState } from 'react';
import { addDoc, collection, serverTimestamp } from 'firebase/firestore';
import { useRouter } from 'next/router';
import validURL from 'utils/checkURL';
import { db } from 'utils/firebase';

const InputForm = () => {
  const [Name, setName] = useState('');
  const [Type, setType] = useState('');
  const [UK, setUK] = useState('');
  const [US, setUS] = useState('');
  const [Link, setLink] = useState('');
  const [show, setShow] = useState(false);
  const [submit, setSubmit] = useState(false);

  const router = useRouter();
  const languagesCollectionRef = collection(db, 'languages');
  const isURL = validURL(Link);

  // show up sucess notification
  const showNotify = () => {
    const shows = setTimeout(() => {
      setShow(false);
      router.push('/');
    }, 3000);
    return () => clearTimeout(shows);
  };

  // set up errors notification
  const errors = { name: '', type: '', link: '', UK: '', US: '' };
  if (submit) {
    if (!Name) {
      errors.name = '*Word is required !';
    }
    if (!Link) {
      errors.link = '*Link is required !';
    } else if (!isURL) {
      errors.link = '*This is not a valid link format !';
    }
    if (!Type) {
      errors.type = '*Type is required !';
    } if (!UK) {
      errors.UK = '*Pronounce in UK is required !';
    } if (!US) {
      errors.US = '*Pronounce in UK is required !';
    }
  }

  // add new word
  const createWord = async (e: { preventDefault: () => void; }) => {
    setSubmit(true);
    if (!Name || !Type || !UK || !US || !Link) {
      e.preventDefault();
    }
    if (Name && Type && UK && US && isURL) {
      e.preventDefault();
      await addDoc(languagesCollectionRef, {
        Name,
        Type,
        UK,
        US,
        Link,
        createAt: serverTimestamp()
      });
      setShow(true);
      showNotify();
      window.scrollTo(0, 0);
    }
  };

  return (
    <div>
      <div className={show ? 'notify' : 'hide'}>
        <div className="notify-text">
          <h5 className="notify-head">Sucess</h5>
          <h6>New word added!</h6>
        </div>
        <div
          onClick={() => setShow(false)}
          onKeyDown={() => setShow(false)}
          className="notify-control"
          role="button"
          tabIndex={0}
        >X
        </div>
      </div>

      <form className="input-form" id="form">
        <h1 className="form-title">Add new word</h1>

        <h6 className="form-label">Name: <span className="error">{errors.name}</span></h6>
        <input
          onChange={(e) => setName(e.target.value)}
          type="text"
          name="name"
          placeholder="word..."
          className="form-control"
        />

        <h6 className="form-label">Type: <span className="error">{errors.type}</span></h6>
        <input
          onChange={(e) => setType(e.target.value)}
          name="type"
          placeholder="type..."
          type="text"
          className="form-control"
        />

        <h6 className="form-label">Pronunciation in UK: <span className="error">{errors.UK}</span></h6>
        <input
          onChange={(e) => setUK(e.target.value)}
          name="UK"
          placeholder="pronunciation in UK..."
          type="text"
          className="form-control"
        />

        <h6 className="form-label">Pronunciation in US: <span className="error">{errors.US}</span></h6>
        <input
          onChange={(e) => setUS(e.target.value)}
          name="US"
          placeholder="pronunciation in US..."
          type="text"
          className="form-control"
        />

        <h6 className="form-label">Link: <span className="error">{errors.link}</span></h6>
        <input
          onChange={(e) => setLink(e.target.value)}
          name="link"
          placeholder="link to dictionary..."
          className="form-control"
        />

        <button
          onClick={createWord}
          type="submit"
          value="submit"
          className="btn btn-primary btn-block mb-4 form-btn"
        >
          Add
        </button>
      </form>
    </div>
  );
};

export default InputForm;
