This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started
<b>step 1</b>:
<br>
git clone in branch dev into local
<br>
<b>step 2:</b>
<br>
To install all the dependencies listed within package.json in the local node_modules folder: .
<br>
<b>yarn install</b> 
<br>
<b>step 3:</b>
<br>
create your new firebase on <a href="https://console.firebase.google.com">https://console.firebase.google.com</a>
<br>
<b>step 4:</b>
change your information firebase inside file .env.local
<br>
<b>Step 5:</b> <br>
Go to <a href="https://console.firebase.google.com">https://console.firebase.google.com</a> <br>
FirestoreDatabase -> Rules -> change 'allow' to 'allow: read, write: if true; <br>
in tab Data, start colection 'languages' <br>
<b>step 6:</b>
open terminal is root folder contain src folder:
<br>
run the development server:
```bash
npm run dev
# or
yarn dev
```
```bash
run the production:
npm run build
# or
yarn build

Open [http://localhost:3000/languages] or (http://localhost:3000) with your browser to see the result.

