import React from 'react';
import Link from 'next/link';

const Header = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      {/* Container wrapper */}
      <div className="container-fluid">
        {/* Toggle button */}
        <button className="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <i className="fas fa-bars" />
        </button>
        {/* Collapsible wrapper */}
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          {/* Navbar brand */}
          <Link className="mt-2 mt-lg-0" href="/">
            <img
              className="logo"
              src="https://png.pngtree.com/element_our/20200702/ourlarge/pngtree-cartoon-blue-open-book-illustration-image_2286285.jpg"
              height={60}
              alt="MDB Logo"
              loading="lazy"
            />
          </Link>
          {/* Left links */}
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <h1 className="brand-name">English Volcabulary</h1>
            </li>
          </ul>
          {/* Left links */}
        </div>
        {/* Collapsible wrapper */}
        {/* Right elements */}
        <div className="d-flex align-items-center">
          <li className="nav-item">
            <Link className="nav-link" href="/singin">Login</Link>
          </li>
        </div>
        {/* Right elements */}
      </div>
      {/* Container wrapper */}
    </nav>
  );
};
export default Header;
