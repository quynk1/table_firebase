import React from 'react';

const Footer = () => {
  return (
    <footer className="text-center text-lg-start bg-light text-muted">
      <div className="text-center p-4" style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
        © Copyright:
        <h4 className="text-reset fw-bold">quynk@appota.com</h4>
        <h4 className="text-reset fw-bold">hiennm@appota.com</h4>
      </div>
      {/* Copyright */}
    </footer>
  );
};

export default Footer;
