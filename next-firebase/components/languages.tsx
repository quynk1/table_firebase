/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import { collection, getDocs, QueryDocumentSnapshot, DocumentData, query, orderBy } from 'firebase/firestore';
import { MDBDataTable } from 'mdbreact';
import Link from 'next/link';
import SpinnerPage from 'components/Spinner';
import { db } from 'utils/firebase';

const Languages = () => {
  const languagesCollectionRef = collection(db, 'languages');
  const q = query(languagesCollectionRef, orderBy('createAt', 'desc'));
  const [languages, setLanguages] = useState<QueryDocumentSnapshot<DocumentData>[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  const getLanguages = async () => {
    const querySnapShot = await getDocs(q);
    const result: QueryDocumentSnapshot<DocumentData>[] = [];
    querySnapShot.forEach((snapshot) => {
      result.push(snapshot);
    });
    setLanguages(result);
  };

  useEffect(() => {
    // get data from firebase
    getLanguages();
    // reset Loading
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, []);

  const newArr = languages.map((lang) =>
    (
      { ...lang.data(),
        Link:
  <a href={`${lang.data().Link}`} target="_blank" rel="noreferrer">
    <span>{lang.data().Link}</span>
  </a>
      }
    ));

  const data = {
    columns: [
      {
        label: 'Name',
        field: 'Name',
        sort: 'asc',
        width: 100
      },
      {
        label: 'Type',
        field: 'Type',
        sort: 'asc',
        width: 30
      },
      {
        label: 'UK',
        field: 'UK',
        sort: 'disabled',
        width: 50
      },
      {
        label: 'US',
        field: 'US',
        sort: 'disabled',
        width: 50
      },
      {
        label: 'Link',
        field: 'Link',
        sort: 'disabled',
        width: 100
      }
    ],
    rows: newArr
  };
  return (
    <div>
      <div className="table-wrapper">
        <Link href="/addword">
          <button type="button" className="btn btn-outline-success" data-mdb-ripple-color="dark">
            <i className="fas fa-plus-circle" />
          </button>
        </Link>
        {
          loading ? (
            <SpinnerPage />
          ) : <MDBDataTable
            striped
            bordered
            small
            data={data}
          />
        }

      </div>
    </div>
  );
};
export default Languages;
