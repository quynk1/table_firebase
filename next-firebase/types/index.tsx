export interface TableTypes {
    Id?: string;
    Name: string;
    Type: string;
    UK: string;
    US: string;
    Link: string;
}
